import Http from "./http";

import { CardsResponse } from "./cards.types";

const API = 'https://api.punkapi.com/v2';

export const getCards = (page: number): CardsResponse => Http.get(`${API}/beers/?page=${page}&per_page=10`);