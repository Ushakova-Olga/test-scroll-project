import { AxiosResponse } from "axios";

type Volume = {
    value: number,
    unit: string
};

type Temp = {
    value: number,
    unit: string
};

type Ingridient = {
    name: string,
    amount: {
        value: number,
        unit: string
    }
};

export type CardsItem = {
    id: number,
    name: string,
    tagline: string,
    first_brewed: string,
    description: string,
    image_url: string,
    abv: number,
    ibu: number,
    target_fg: number,
    target_og: number,
    ebc: number,
    srm: number,
    ph: number,
    attenuation_level: number,
    volume: Volume,
    boil_volume: Volume,
    method: {
        mash_temp: { temp: Temp, duration: number }[],
        fermentation: { temp: Temp },
    },
    ingredients: Record<string, Ingridient[]>,
    food_pairing: string[],
    brewers_tips: string,
    contributed_by: string
};

export type CardsData = CardsItem[];


export type CardsResponse = Promise<AxiosResponse<CardsData>>;
