
import axios from "axios";

const Http = axios.create();

Http.interceptors.response.use(
    (response) => response,
    (error) => {
        const status = error.response ? error.response.status : 408;

        if (status >= 500) {
            console.error(error);
        }

        return Promise.reject(error);
    }
);

export default Http;