import { FC } from 'react'

import styles from './styles.module.scss';

export type CardItem = {
  id: number,
  name: string,
  description: string
}

const Card: FC<CardItem> = ({ id, name, description }) => (
    <div className={styles.card}>
      <p className={styles.name}>
        <span className={styles.id}>{id}</span>
        {name}
      </p>
      <p className={styles.description}>{description}</p>
    </div>
);

export default Card;
