import { FC, ReactNode } from 'react';
import styles from './styles.module.scss';

type ListProps = {
  children: ReactNode;
};

const List: FC<ListProps> = ({ children }) => (
  <div className={styles.list}>
    {children}
  </div>
);


export default List;
