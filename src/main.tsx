import React from 'react'
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import Main from './pages/Main';
import configureStore from './store';
import './index.css'

export type AppDispatch = typeof configureStore.dispatch;

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Provider store={configureStore}>
      <Main />
    </Provider>
  </React.StrictMode>,
)
