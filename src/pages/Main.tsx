import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import InfiniteScroll from 'react-infinite-scroll-component';
import List from '../components/List/List';
import Card from '../components/Card/Card';
import { CardsState } from '../store/types';
import { OperationCards } from '../store/actions';
import { AppDispatch } from '../main';

function Main() {
  const dispatch = useDispatch<AppDispatch>();

  const { page, cards } = useSelector((state: CardsState) => state);


  useEffect(()=>{
    if (!cards.length) {
      dispatch(OperationCards.getCardsData(page));
    }
    return(() => { 
      dispatch(OperationCards.resetData());
     })
  },[])

  return(<InfiniteScroll
     dataLength={cards.length}
     next={() => {
      dispatch(OperationCards.getCardsData(page));
     }}
     hasMore={true}
     loader={<p>load...</p>}>
      <List>{cards.map(({ id, name, description }) => (
        <Card key={id} id={id} name={name} description={description} />
        ))}
      </List>
    
   </InfiniteScroll>);
}

export default Main
