import { Dispatch } from 'redux';
import { getCards } from '../api/cards';
import { CardsData } from '../api/cards.types';
import { CardsActionSuccess, CardsActionFailure, CardsActionReset } from './types';
import { GET_CARDS_SUCCESS, GET_CARDS_FAILURE, RESET_CARDS } from './types';

const getCardsSuccess = (data: CardsData, page: number): CardsActionSuccess => {
  return {
    type: GET_CARDS_SUCCESS,
    payload: { data, page },
  };
};

const getCardsFailure = (error: string): CardsActionFailure => {
  return {
    type: GET_CARDS_FAILURE,
    payload: error,
  };
};

const reset = (): CardsActionReset => {
  return {
    type: RESET_CARDS,
  };
};

async function getCardsAsync(page: number, dispatch: Dispatch) {
  try {
    const response = await getCards(page);
    dispatch(getCardsSuccess(response.data, page));
  } catch (err) {
    dispatch(getCardsFailure((err as Error).toString()));
  }
}

const OperationCards = {
  getCardsData: (data: number) => (dispatch: Dispatch) => {
    return getCardsAsync(data, dispatch);
  },
  resetData: () => (dispatch: Dispatch) => {
    dispatch(reset());
  },
};

export { OperationCards };
