import { configureStore } from '@reduxjs/toolkit';

import cardsReducer from './reducer';

export default configureStore({ reducer: cardsReducer });