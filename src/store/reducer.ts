import { Reducer } from "redux";
import { CardsState } from './types';

import { GET_CARDS_SUCCESS, GET_CARDS_FAILURE, RESET_CARDS } from './types';


const initialState: CardsState = {
  error: '',
  cards: [],
  page: 1,
};

const cardsReducer: Reducer<CardsState> = (state = initialState, action) => {
  switch (action.type) {
    case GET_CARDS_FAILURE:
      return {
        ...initialState,
        error: action.payload,
      };

    case GET_CARDS_SUCCESS:
      return {
        ...state,
        cards: action.payload.page === 1 ? [...action.payload.data] : [...state.cards, ...action.payload.data],
        page: action.payload.page + 1,
        error: '',
      };

    case RESET_CARDS:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default cardsReducer;