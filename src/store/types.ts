import { CardsData } from '../api/cards.types';

export const GET_CARDS_SUCCESS = 'GET_CARDS_SUCCESS';
export const GET_CARDS_FAILURE = 'GET_CARDS_FAILURE';
export const RESET_CARDS = 'RESET_CARDS';

export type CardsState = {
  error: string,
  cards: CardsData,
  page: number,
}

export type CardsActionSuccess = { type: typeof GET_CARDS_SUCCESS; payload: { data: CardsData, page: number } };
export type CardsActionFailure = { type: typeof GET_CARDS_FAILURE; payload: string };
export type CardsActionReset = { type: typeof RESET_CARDS; };
